from pygame import event, QUIT, KEYDOWN, K_SPACE
from .a_star import AStar


def check_events(a_star: AStar) -> bool:
    for e in event.get():
        if e.type == QUIT:
            return False
        elif e.type == KEYDOWN:
            if e.key == K_SPACE:
                a_star.reset()
    return True

