from .settings import TileType, MAZE_LINES, MAZE_COLUMNS
from random import choice


def random_type(odd: int = 3) -> TileType:
    if choice(range(10)) in range(odd):
        return TileType.WALL
    return TileType.VALID_TILE


def diagonal_distance(start: tuple, end: tuple) -> float:
    return ((end[0] - start[0])**2 + (end[1] - start[1])**2)**(1/2)


class Tile:

    def __init__(self, pos: tuple):
        self.pos = pos
        self.pai: tuple = ()
        self.neighbor = []
        self.type = random_type()
        self.f: float = 0
        self.g: float = 0
        self.h: float = 0

    def set_neighbor(self, grid):
        if not self.neighbor:
            i, j = self.pos
            if j + 1 < MAZE_COLUMNS and grid[i][j+1].type != TileType.WALL:
                self.neighbor.append((i, j+1))
            if j - 1 >= 0 and grid[i][j-1].type != TileType.WALL:
                self.neighbor.append((i, j-1))
            if i + 1 < MAZE_LINES and grid[i+1][j].type != TileType.WALL:
                if grid[i + 1][j].type != TileType.WALL:
                    self.neighbor.append((i+1, j))
                if j + 1 < MAZE_COLUMNS and grid[i+1][j+1].type != TileType.WALL:
                    self.neighbor.append((i+1, j+1))
                if j - 1 >= 0 and grid[i+1][j-1].type != TileType.WALL:
                    self.neighbor.append((i+1, j-1))
            if i - 1 >= 0 and grid[i-1][j].type != TileType.WALL:
                if grid[i-1][j].type != TileType.WALL:
                    self.neighbor.append((i-1, j))
                if j - 1 >= 0 and grid[i-1][j-1].type != TileType.WALL:
                    self.neighbor.append((i-1, j-1))
                if j + 1 < MAZE_COLUMNS and grid[i-1][j+1].type != TileType.WALL:
                    self.neighbor.append((i-1, j+1))



