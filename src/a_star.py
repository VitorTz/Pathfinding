from .settings import START, END, TileType, TILE_HEIGHT, TILE_WIDTH
from .tile import Tile, diagonal_distance
from .grid import init_grid, draw_grid, get_tile, draw_tile, Rect, change_type, Surface


class AStar:
    __grid: list[list[Tile]]
    __current_tile: Tile
    __final_tile: Tile
    __active: bool
    __open_set: list[Tile]
    __close_set: set[Tile]
    __rect = Rect(0, 0, TILE_WIDTH, TILE_HEIGHT)

    def __init__(self, surface: Surface):
        self.__surface = surface
        self.reset()

    def reset(self):
        self.__grid = init_grid()
        self.__active = True
        self.__open_set = [get_tile(START, self.__grid)]
        self.__close_set = set()
        self.__final_tile = get_tile(END, self.__grid)

    def heuristic(self, tile: Tile):
        """Define as informações do tile"""
        tile.pai = self.__current_tile.pos
        tile.g = diagonal_distance(tile.pos, self.__final_tile.pos)
        tile.h = diagonal_distance(self.__current_tile.pos, tile.pos)
        tile.f = tile.g + tile.h

    def draw_tile_hierarchy(self, tile: Tile):
        """Desenha os tiles relacionados com o tile recebido"""
        while tile.pai:
            draw_tile(self.__surface, TileType.CURRENT_PATH, self.__rect, tile.pos)
            tile = get_tile(tile.pai, self.__grid)

    def get_current_tile_neighbors(self):
        if not self.__current_tile.neighbor:
            self.__current_tile.set_neighbor(self.__grid)
            self.__current_tile.neighbor = [get_tile(pos, self.__grid) for pos in self.__current_tile.neighbor]

    def check_neighbors(self):
        """Adiciona os vizinhos válidos do tile atual e desenha o grau de parentesco de cada um"""
        self.get_current_tile_neighbors()  # Consegue os vizinhos do tile atual

        for tile in self.__current_tile.neighbor:
            if tile.type != TileType.WALL and tile not in self.__close_set and tile not in self.__open_set:
                self.heuristic(tile)  # Define f, g e h
                self.draw_tile_hierarchy(tile)  # Desenha os tiles relacionados com o tile atual
                self.__open_set.append(tile)
                if self.check_finish_condition(tile):
                    break
                # Muda o tile do tile, o tile de tipo TESTED_TILE tem a cor branca
                change_type(self.__grid, tile.pos, TileType.TESTED_TILE)
        self.__open_set.sort(key=lambda i: i.f)

    def set_final_tile(self, tile_pai: Tile, tile: Tile) -> None:
        self.__final_tile = tile
        self.__final_tile.pai = tile_pai.pos

    def check_finish_condition(self, tile: Tile) -> bool:
        """Verifica se o tile é o tile de destino, caso sim, encerra a procura e define o tile pai correto
        para o tile de destino"""
        if tile.type == TileType.END:
            self.set_final_tile(self.__current_tile, tile)
            self.__active = False
            return True

    def do_path_finding(self):
        """Realiza a procura pelo caminho mais curto"""
        if self.__open_set and self.__active:
            self.__current_tile = self.__open_set.pop(0)
            self.__close_set.add(self.__current_tile)
            self.check_neighbors()
        else:
            self.draw_tile_hierarchy(self.__final_tile)

    def run(self) -> None:
        draw_grid(self.__grid, self.__surface, self.__rect)
        self.do_path_finding()
