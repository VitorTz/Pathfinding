from pygame import Rect, Surface, draw
from .tile import Tile
from .settings import TileType, MAZE_LINES, END, START, MAZE_COLUMNS, TILE_WIDTH, TILE_HEIGHT, COLORS


def set_start_end(grid: list[list[Tile]]) -> None:
    grid[START[0]][START[1]].type = TileType.START
    grid[END[0]][END[1]].type = TileType.END


def init_grid() -> list[list[Tile]]:
    grid = [None] * MAZE_LINES
    for i in range(MAZE_LINES):
        grid[i] = [None] * MAZE_COLUMNS
        for j in range(MAZE_COLUMNS):
            grid[i][j] = Tile((i, j))
    set_start_end(grid)
    return grid


def set_screen_position(i: int, j: int, rect: Rect):
    rect.left = TILE_WIDTH * j
    rect.top = TILE_HEIGHT * i


def draw_tile(surface: Surface, tile_type: TileType, rect: Rect, pos: tuple[int, int]) -> None:
    set_screen_position(pos[0], pos[1], rect)
    draw.rect(surface, COLORS[tile_type], rect)


def draw_grid(grid: list[list[Tile]], surface: Surface, rect: Rect) -> None:
    for i in range(MAZE_LINES):
        for j in range(MAZE_COLUMNS):
            draw_tile(surface, grid[i][j].type, rect, (i, j))


def get_tile(pos: tuple, grid: list[list[Tile]]) -> Tile:
    if pos[0] in range(MAZE_LINES) and pos[1] in range(MAZE_COLUMNS):
        return grid[pos[0]][pos[1]]


def change_type(grid: list[list[Tile]], pos: tuple, tile_type: TileType) -> None:
    grid[pos[0]][pos[1]].type = tile_type
