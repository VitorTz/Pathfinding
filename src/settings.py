from enum import Enum, auto


SURFACE_WIDTH = 1280
SURFACE_HEIGHT = 800
SURFACE_TITLE = "Pathfinding"
TILE_WIDTH = TILE_HEIGHT = 5
MAZE_LINES = SURFACE_HEIGHT // TILE_HEIGHT
MAZE_COLUMNS = SURFACE_WIDTH // TILE_WIDTH

START = (0, 0)
END = (MAZE_LINES-1, MAZE_COLUMNS-1)


class TileType(Enum):

    WALL = auto()
    VALID_TILE = auto()
    TESTED_TILE = auto()
    CURRENT_PATH = auto()
    START = auto()
    END = auto()


COLORS = {
    TileType.WALL: (0, 0, 0),  # black
    TileType.VALID_TILE: (41, 42, 53),  # grey
    TileType.CURRENT_PATH: (230, 0, 0),  # red
    TileType.TESTED_TILE: (230, 230, 230),  # white
    TileType.START: (252, 148, 3),  # orange
    TileType.END: (195, 0, 255)  # purple
}

