import pygame
from sys import exit
from src.settings import SURFACE_TITLE, SURFACE_WIDTH, SURFACE_HEIGHT
from src.check_events import check_events
from src.a_star import AStar


def main():
    pygame.init()
    pygame.display.set_caption(SURFACE_TITLE)
    surface = pygame.display.set_mode((SURFACE_WIDTH, SURFACE_HEIGHT))
    a_star = AStar(surface)

    while check_events(a_star):
        a_star.run()
        pygame.display.update()

    pygame.quit()
    exit()


if __name__ == "__main__":
    main()

